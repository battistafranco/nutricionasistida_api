'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PatientSchema = Schema({
    nombre: String,
    apellido: String,
    descripcion: String, 
    obraSocial: String,
    image: String
});

module.exports = mongoose.model('Patient', PatientSchema);
