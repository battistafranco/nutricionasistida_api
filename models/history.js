'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var HistorySchema = Schema({
    titulo: String,
    descripcion: String,
    fecha: Date,
    image: String,
    patient: {type: Schema.ObjectId, ref: 'Patient'}
});

module.exports = mongoose.model('History', HistorySchema);