'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 5000;
mongoose.Promise = global.Promise;
var mongoDB = process.env.MONGODB_URI || 'mongodb://usr_nutricion:usrnutricion@ds139942.mlab.com:39942/nutricion';
mongoose.connect(mongoDB, (err, res) => {
    if (err) {
        throw err;
    }
    else {
        console.log("Conn to DB OK");
        app.listen(port, function () {
            console.log("Server listening en ds139942.mlab.com:39942/" + port)
        });
    }
});