'use strict'

var express = require('express');
var PatientController = require('../controllers/patient');
var api = express.Router();
var md_auth = require('../middlewares/authenticated');
var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: './uploads/patients'});

api.get('/patient/:id', md_auth.ensureAuth, PatientController.getPatient);
api.get('/patients/:page?', md_auth.ensureAuth, PatientController.getPatients);
api.post('/patient', md_auth.ensureAuth, PatientController.savePatient);
api.put('/update-patient/:id', md_auth.ensureAuth, PatientController.updatePatient);
api.delete('/delete-patient/:id', md_auth.ensureAuth, PatientController.deletePatient);
api.post('/upload-image-patient/:id',[md_auth.ensureAuth, md_upload], PatientController.uploadImage);
api.get('/get-image-patient/:imageFile', PatientController.getImageFile);


module.exports = api;
