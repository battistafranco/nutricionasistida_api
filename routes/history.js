'use strict'

var express = require('express');
var HistoryController = require('../controllers/history');
var api = express.Router();
var md_auth = require('../middlewares/authenticated');
var multipart = require('connect-multiparty');
var md_upload = multipart({uploadDir: './uploads/historys'});

api.get('/history/:id', md_auth.ensureAuth, HistoryController.getHistory);
api.get('/historys/:patient?', md_auth.ensureAuth, HistoryController.getHistorys);
api.post('/history', md_auth.ensureAuth, HistoryController.saveHistory);
api.put('/update-history/:id', md_auth.ensureAuth, HistoryController.updateHistory);
api.delete('/delete-history/:id', md_auth.ensureAuth, HistoryController.deleteHistory);
api.post('/upload-image-history/:id',[md_auth.ensureAuth, md_upload], HistoryController.uploadImage);
api.get('/get-image-history/:imageFile', HistoryController.getImageFile);

module.exports = api;
