'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//cargar rutas
var user_routes = require('./routes/user');
var patient_routes = require('./routes/patient');
var history_routes = require('./routes/history');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//config headers http
app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
})
//rutas base
app.use('/api', user_routes);
app.use('/api', patient_routes);
app.use('/api', history_routes);

module.exports = app;
