'use strict'

var fs = require('fs');
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var Patient = require('../models/patient');
var History = require('../models/history');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');

function getPatient(req, res) {
    var patientId = req.params.id;
    Patient.findById(patientId, (err, patient) => {
        if (err) {
            res.status(500).send({ message: 'Error en get paciente' });
        }
        else {
            if (!patient) {
                res.status(404).send({ message: 'Paciente no encontrado' });
            } else {
                res.status(200).send({ patient });
            }
        }
    });

}

function getPatients(req, res) {
    if (req.params.page) {
        var page = req.params.page;
    } else {
        var page = 1;
    }
    var page = req.params.page;
    var itemsPerPage = 10;
    Patient.find().sort('name').paginate(page, itemsPerPage, (err, patients, total) => {
        if (err) {
            res.status(500).send({ message: 'Error en get pacientes' });
        }
        else {
            if (!patients) {
                res.status(404).send({ message: 'No existen pacientes' });
            } else {
                return res.status(200).send({
                    patients: patients,
                    total_items: total
                });
            }
        }
    })
}

function updatePatient(req, res) {
    var patientId = req.params.id;
    var update = req.body;
    Patient.findByIdAndUpdate(patientId, update, (err, patientUpdated) => {
        if (err) {
            res.status(500).send({ message: 'Error al actualizar el paciente.' });
        }
        else {
            if (!patientUpdated) {
                res.status(404).send({ message: 'No se ha podido actualizar el paciente.' });
            }
            else {
                res.status(200).send({ patient: patientUpdated });
            }
        }
    });
};

function savePatient(req, res) {
    var a = new Patient();
    var params = req.body;

    a.nombre = params.nombre;
    a.apellido = params.apellido;
    a.descripcion = params.descripcion;
    a.obraSocial = params.obraSocial;
    a.image = 'null';

    a.save((err, patientStored) => {
        if (err) {
            res.status(500).send({ message: "Error al guarda el paciente." });
        } else {
            if (!patientStored) {
                res.status(404).send({ message: "El paciente no ha sido guardado." });
            }
            else {
                res.status(200).send({ patient: patientStored });
            }
        }
    });
}

function deletePatient(req, res) {
    var patientId = req.params.id;
    Patient.findByIdAndRemove(patientId, (err, patientRemoved) => {
        if (err) {
            res.status(500).send({ message: 'Error al eliminar el paciente.' });
        } else {
            if (!patientRemoved) {
                res.status(404).send({ message: 'El patiente no ha sido eliminado.' });
            }
            else {
                History.find({ patient: patientRemoved._id }).remove((err, historyRemoved) => {
                    if (err) {
                        res.status(500).send({ message: 'Error al eliminar Historia Clínica.' });
                    } else {
                        if (!historyRemoved) {
                            res.status(404).send({ message: 'Historia Clínica no ha sido eliminado.' });
                        }
                        else {
                            res.status(200).send({ patient: patientRemoved });
                        }
                    }
                });
            }
        }

    });
}

function uploadImage(req, res) {
    var patientId = req.params.id;
    var file_name = 'No_Subido';

    if (req.files) {
        var file_path = req.files.image.path;
        var file_split = file_path.split('\\');
        var file_name = file_split[2];
        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif') {
            Patient.findByIdAndUpdate(patientId, { image: file_name }, (err, patientUpdated) => {
                if (!patientUpdated) {
                    res.status(404).send({ message: 'No se ha podido actualizar el paciente.' });
                }
                else {
                    res.status(200).send({ user: patientUpdated });
                }
            });
        } else {
            res.status(200).send({ message: 'Extension del archivo invalida.' });
        }
    } else {
        res.status(200).send({ message: 'No ha subido ninguna imagen' });
    }
}

function getImageFile(req, res) {
    var imageFile = req.params.imageFile;
    var pathFile = './uploads/patients/' + imageFile;
    fs.exists(pathFile, function (exists) {
        if (exists)
        { res.sendFile(path.resolve(pathFile)) }
        else {
            res.status(200).send({ message: 'No existe la imagen' });
        }
    });
};

module.exports = {
    getPatient,
    savePatient,
    getPatients,
    updatePatient,
    deletePatient,
    uploadImage,
    getImageFile
};