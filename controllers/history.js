'use strict'

var fs = require('fs');
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var Patient = require('../models/patient');
var History = require('../models/history');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');

function getHistory(req, res) {
    var historyId = req.params.id;
    History.findById(historyId).populate({ path: 'patient' }).exec((err, history) => {
        if (err) {
            res.status(500).send({ message: 'Error en buscar historia clínica' });
        }
        else {
            if (!history) {
                res.status(404).send({ message: 'Historia clínica no encontrado' });
            } else {
                res.status(200).send({ history });
            }
        }
    });

}

function getHistorys(req, res) {

    var patientId = req.params.patient;
    if (!patientId) {
        var find = History.find({}).sort('title');
    } else {
        var find = History.find({ patient: patientId }).sort('fecha');
    }
    find.populate({ path: 'patient' }).exec((err, historys) => {

        if (err) {
            res.status(500).send({ message: 'Error en buscar Historias clínicas' });
        }
        else {
            if (!historys) {
                res.status(404).send({ message: 'No existen Historias clínicas ' });
            } else {
                return res.status(200).send({ historys });
            }
        }
    });
}

function updateHistory(req, res) {
    var historyId = req.params.id;
    var update = req.body;
    History.findByIdAndUpdate(historyId, update, (err, historyUpdated) => {
        if (err) {
            res.status(500).send({ message: 'Error al actualizar Historia clínica.' });
        }
        else {
            if (!historyUpdated) {
                res.status(404).send({ message: 'No se ha podido actualizar Historia clínica.' });
            }
            else {
                res.status(200).send({ history: historyUpdated });
            }
        }
    });
};

function saveHistory(req, res) {
    var a = new History();
    var params = req.body;

    a.titulo = params.titulo;
    a.descripcion = params.descripcion;
    a.fecha = params.fecha;
    a.image = 'null';
    a.patient = params.patient;

    a.save((err, historyStored) => {
        if (err) {
            res.status(500).send({ message: "Error al guardar Historia clínica." });
        } else {
            if (!historyStored) {
                res.status(404).send({ message: "Historia clínica no ha sido guardada." });
            }
            else {
                res.status(200).send({ history: historyStored });
            }
        }
    });
}

function deleteHistory(req, res) {
    var historyId = req.params.id;
    History.findByIdAndRemove(historyId, (err, historyRemoved) => {
        if (err) {
            res.status(500).send({ message: 'Error al eliminar Historia clínica.' });
        } else {
            if (!historyRemoved) {
                res.status(404).send({ message: 'Historia clínica no ha sido eliminada.' });
            }
            else {
                res.status(200).send({ history: historyRemoved });
            }
        }

    });
}

function uploadImage(req, res) {
    var historyId = req.params.id;
    var file_name = 'No_Subido';

    if (req.files) {
        var file_path = req.files.image.path;
        var file_split = file_path.split('\\');
        var file_name = file_split[2];
        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif') {
            History.findByIdAndUpdate(historyId, { image: file_name }, (err, historyUpdated) => {
                if (!historyUpdated) {
                    res.status(404).send({ message: 'No se ha podido actualizar Historia clínica.' });
                }
                else {
                    res.status(200).send({ user: historyUpdated });
                }
            });
        } else {
            res.status(200).send({ message: 'Extension del archivo invalida.' });
        }
    } else {
        res.status(200).send({ message: 'No ha subido ninguna imagen' });
    }
}

function getImageFile(req, res) {
    var imageFile = req.params.imageFile;
    var pathFile = './uploads/historys/' + imageFile;
    fs.exists(pathFile, function (exists) {
        if (exists)
        { res.sendFile(path.resolve(pathFile)) }
        else {
            res.status(200).send({ message: 'No existe la imagen' });
        }
    });
};

module.exports = {
    getHistory,
    saveHistory,
    getHistorys,
    updateHistory,
    deleteHistory,
    uploadImage,
    getImageFile
};